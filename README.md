* https://learn.hashicorp.com/vault/kubernetes/minikube#initialize-and-unseal-vault
* https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28063
* https://docs.gitlab.com/ee/user/clusters/applications.html
* https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
* https://gitlab.com/groups/gitlab-org/-/epics/2103 (specifically https://gitlab.com/groups/gitlab-org/-/epics/2103#note_314390103)

`vault kv put secret/demo/config username="demo-user" password="demo-password"`

`vault policy write vault-demo - <<EOF`

```
# Policy name: vault-demo
#
# Read-only perms on 'secret/demo/config/*' path
path "secret/demo/config/*" {
  capabilities = [ "read" ]
}
EOF
```

`vault write auth/jwt/role/vault-demo - <<EOF`
```
{
  "role_type": "jwt",
  "policies": ["vault-demo"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "19133186",
    "ref": "master",
    "ref_type": "branch"
  }
}
EOF
```

```
vault write auth/jwt/config \
    jwks_url="https://gitlab.com/-/jwks" \
    bound_issuer="gitlab.com"
```
